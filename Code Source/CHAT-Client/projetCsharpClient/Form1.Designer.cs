﻿namespace projetCsharpClient
{
    partial class ChatApp
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.LogGoupBox = new System.Windows.Forms.GroupBox();
            this.logoutButton = new System.Windows.Forms.Button();
            this.loginButton = new System.Windows.Forms.Button();
            this.textMdp = new System.Windows.Forms.TextBox();
            this.textPort = new System.Windows.Forms.TextBox();
            this.textIp = new System.Windows.Forms.TextBox();
            this.textPseudo = new System.Windows.Forms.TextBox();
            this.mdpLabel = new System.Windows.Forms.Label();
            this.portLabel = new System.Windows.Forms.Label();
            this.ipLabel = new System.Windows.Forms.Label();
            this.pseudoLabel = new System.Windows.Forms.Label();
            this.ChatBox = new System.Windows.Forms.GroupBox();
            this.textCanal = new System.Windows.Forms.TextBox();
            this.textMessage = new System.Windows.Forms.TextBox();
            this.sendButton = new System.Windows.Forms.Button();
            this.chatTextBox = new System.Windows.Forms.ListBox();
            this.canalLabel = new System.Windows.Forms.Label();
            this.LogGoupBox.SuspendLayout();
            this.ChatBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // LogGoupBox
            // 
            this.LogGoupBox.Controls.Add(this.logoutButton);
            this.LogGoupBox.Controls.Add(this.loginButton);
            this.LogGoupBox.Controls.Add(this.textMdp);
            this.LogGoupBox.Controls.Add(this.textPort);
            this.LogGoupBox.Controls.Add(this.textIp);
            this.LogGoupBox.Controls.Add(this.textPseudo);
            this.LogGoupBox.Controls.Add(this.mdpLabel);
            this.LogGoupBox.Controls.Add(this.portLabel);
            this.LogGoupBox.Controls.Add(this.ipLabel);
            this.LogGoupBox.Controls.Add(this.pseudoLabel);
            this.LogGoupBox.Location = new System.Drawing.Point(25, 13);
            this.LogGoupBox.Name = "LogGoupBox";
            this.LogGoupBox.Size = new System.Drawing.Size(356, 107);
            this.LogGoupBox.TabIndex = 0;
            this.LogGoupBox.TabStop = false;
            this.LogGoupBox.Text = "LogIn";
            // 
            // logoutButton
            // 
            this.logoutButton.Location = new System.Drawing.Point(247, 78);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(75, 23);
            this.logoutButton.TabIndex = 10;
            this.logoutButton.Text = "Logout";
            this.logoutButton.UseVisualStyleBackColor = true;
            this.logoutButton.Click += new System.EventHandler(this.LogoutButton_Click);
            // 
            // loginButton
            // 
            this.loginButton.Location = new System.Drawing.Point(247, 49);
            this.loginButton.Name = "loginButton";
            this.loginButton.Size = new System.Drawing.Size(75, 23);
            this.loginButton.TabIndex = 9;
            this.loginButton.Text = "Login";
            this.loginButton.UseVisualStyleBackColor = true;
            this.loginButton.Click += new System.EventHandler(this.LoginButton_Click);
            // 
            // textMdp
            // 
            this.textMdp.Location = new System.Drawing.Point(247, 20);
            this.textMdp.Name = "textMdp";
            this.textMdp.Size = new System.Drawing.Size(70, 20);
            this.textMdp.TabIndex = 8;
            // 
            // textPort
            // 
            this.textPort.Location = new System.Drawing.Point(76, 68);
            this.textPort.Name = "textPort";
            this.textPort.Size = new System.Drawing.Size(70, 20);
            this.textPort.TabIndex = 7;
            this.textPort.Text = "8080";
            // 
            // textIp
            // 
            this.textIp.Location = new System.Drawing.Point(76, 46);
            this.textIp.Name = "textIp";
            this.textIp.Size = new System.Drawing.Size(70, 20);
            this.textIp.TabIndex = 6;
            this.textIp.Text = "127.0.0.1";
            // 
            // textPseudo
            // 
            this.textPseudo.Location = new System.Drawing.Point(76, 20);
            this.textPseudo.Name = "textPseudo";
            this.textPseudo.Size = new System.Drawing.Size(70, 20);
            this.textPseudo.TabIndex = 5;
            // 
            // mdpLabel
            // 
            this.mdpLabel.AutoSize = true;
            this.mdpLabel.Location = new System.Drawing.Point(161, 27);
            this.mdpLabel.Name = "mdpLabel";
            this.mdpLabel.Size = new System.Drawing.Size(80, 13);
            this.mdpLabel.TabIndex = 3;
            this.mdpLabel.Text = "Mot de passe : ";
            // 
            // portLabel
            // 
            this.portLabel.AutoSize = true;
            this.portLabel.Location = new System.Drawing.Point(21, 71);
            this.portLabel.Name = "portLabel";
            this.portLabel.Size = new System.Drawing.Size(35, 13);
            this.portLabel.TabIndex = 2;
            this.portLabel.Text = "Port : ";
            // 
            // ipLabel
            // 
            this.ipLabel.AutoSize = true;
            this.ipLabel.Location = new System.Drawing.Point(21, 49);
            this.ipLabel.Name = "ipLabel";
            this.ipLabel.Size = new System.Drawing.Size(26, 13);
            this.ipLabel.TabIndex = 1;
            this.ipLabel.Text = "IP : ";
            // 
            // pseudoLabel
            // 
            this.pseudoLabel.AutoSize = true;
            this.pseudoLabel.Location = new System.Drawing.Point(21, 27);
            this.pseudoLabel.Name = "pseudoLabel";
            this.pseudoLabel.Size = new System.Drawing.Size(49, 13);
            this.pseudoLabel.TabIndex = 0;
            this.pseudoLabel.Text = "Pseudo :";
            // 
            // ChatBox
            // 
            this.ChatBox.Controls.Add(this.textCanal);
            this.ChatBox.Controls.Add(this.textMessage);
            this.ChatBox.Controls.Add(this.sendButton);
            this.ChatBox.Controls.Add(this.chatTextBox);
            this.ChatBox.Controls.Add(this.canalLabel);
            this.ChatBox.Location = new System.Drawing.Point(25, 126);
            this.ChatBox.Name = "ChatBox";
            this.ChatBox.Size = new System.Drawing.Size(356, 277);
            this.ChatBox.TabIndex = 1;
            this.ChatBox.TabStop = false;
            this.ChatBox.Text = "Chat";
            // 
            // textCanal
            // 
            this.textCanal.Location = new System.Drawing.Point(76, 18);
            this.textCanal.Name = "textCanal";
            this.textCanal.Size = new System.Drawing.Size(115, 20);
            this.textCanal.TabIndex = 12;
            this.textCanal.Text = "general";
            // 
            // textMessage
            // 
            this.textMessage.Location = new System.Drawing.Point(24, 227);
            this.textMessage.Name = "textMessage";
            this.textMessage.Size = new System.Drawing.Size(232, 20);
            this.textMessage.TabIndex = 11;
            // 
            // sendButton
            // 
            this.sendButton.Location = new System.Drawing.Point(262, 225);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(75, 23);
            this.sendButton.TabIndex = 11;
            this.sendButton.Text = "send";
            this.sendButton.UseVisualStyleBackColor = true;
            this.sendButton.Click += new System.EventHandler(this.SendButton_Click);
            // 
            // chatTextBox
            // 
            this.chatTextBox.FormattingEnabled = true;
            this.chatTextBox.Location = new System.Drawing.Point(24, 59);
            this.chatTextBox.Name = "chatTextBox";
            this.chatTextBox.Size = new System.Drawing.Size(313, 160);
            this.chatTextBox.TabIndex = 10;
            // 
            // canalLabel
            // 
            this.canalLabel.AutoSize = true;
            this.canalLabel.Location = new System.Drawing.Point(21, 25);
            this.canalLabel.Name = "canalLabel";
            this.canalLabel.Size = new System.Drawing.Size(43, 13);
            this.canalLabel.TabIndex = 4;
            this.canalLabel.Text = "Canal : ";
            // 
            // ChatApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(393, 415);
            this.Controls.Add(this.ChatBox);
            this.Controls.Add(this.LogGoupBox);
            this.Name = "ChatApp";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ChatApp";
            this.LogGoupBox.ResumeLayout(false);
            this.LogGoupBox.PerformLayout();
            this.ChatBox.ResumeLayout(false);
            this.ChatBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox LogGoupBox;
        private System.Windows.Forms.TextBox textMdp;
        private System.Windows.Forms.TextBox textPort;
        private System.Windows.Forms.TextBox textIp;
        private System.Windows.Forms.TextBox textPseudo;
        private System.Windows.Forms.Label canalLabel;
        private System.Windows.Forms.Label mdpLabel;
        private System.Windows.Forms.Label portLabel;
        private System.Windows.Forms.Label ipLabel;
        private System.Windows.Forms.Label pseudoLabel;
        private System.Windows.Forms.GroupBox ChatBox;
        private System.Windows.Forms.Button logoutButton;
        private System.Windows.Forms.Button loginButton;
        private System.Windows.Forms.ListBox chatTextBox;
        private System.Windows.Forms.TextBox textMessage;
        private System.Windows.Forms.Button sendButton;
        private System.Windows.Forms.TextBox textCanal;
    }
}

