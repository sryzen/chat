﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using System.Threading;

namespace projetCsharpClient
{


    public partial class ChatApp : Form
    {
        public ChatApp()
        {
            InitializeComponent();
        }

        Socket clientSocket;
        Thread clientWorker;
        Boolean isConnected = false;

        private void ChatApp_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (clientSocket != null)
            {
                clientSocket.Close();
            }
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            if (!isConnected)
            {
                if (textPseudo.Text == "")
                {
                    chatTextBox.Items.Add("you need a pseudo!");
                    return;
                }
                if (textMdp.Text == "")
                {
                    chatTextBox.Items.Add("you need a password!");
                    return;
                }
                if (textIp.Text == "")
                {
                    chatTextBox.Items.Add("you need an IP!");
                    return;
                }
                if (textPort.Text == "")
                {
                    chatTextBox.Items.Add("you need a port!");
                    return;
                }

                clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                try
                {
                    IPEndPoint endp = new IPEndPoint(IPAddress.Parse(textIp.Text),Convert.ToInt32(textPort.Text));
                    clientSocket.Connect(endp);
                    chatTextBox.Items.Add("Connexion establish!");
                    Byte[] message = System.Text.Encoding.UTF8.GetBytes(textPseudo.Text + "/" + textMdp.Text);
                    int send = clientSocket.Send(message);
                    isConnected = true;
                    clientWorker = new Thread(workerLecture);
                    clientWorker.IsBackground = true;
                    clientWorker.Start();
                    
                }
                catch(Exception ex)
                {
                    chatTextBox.Items.Add("Connection failed. Check ip address and port!");
                }                   
            }
        }

        private void LogoutButton_Click(object sender, EventArgs e)
        {
            if (isConnected)
            {
                clientSocket.Close();
                chatTextBox.Items.Add("Disconnected");
                isConnected = false;
            }

        }

        private void workerLecture()
        {
            while (this.isConnected)
            {
                Byte[] donnee = new byte[255];
                int recu;

                try
                {
                    recu = clientSocket.Receive(donnee);
                    if (recu != 0)
                    {
                        receivetreet(donnee);
                    }
                    Thread.Sleep(100);
                    
                }
                catch (Exception ex)
                {
                // updateChatBox("Connexion Lost");
                }
               
            }
        }

        public void updateChatBox(string message)
        {
            chatTextBox.Invoke(new Action(() => chatTextBox.Items.Add(message)));
        }   

        private void SendButton_Click(object sender, EventArgs e)
        {
            if (isConnected)
            {
                message mess = new message(textMessage.Text, textPseudo.Text, textCanal.Text);
                mess.sendMessage(clientSocket);
            }
        }

        public void receivetreet( Byte[] bytes)
        {
            message mess = new message();
            String tempString;
            String[] tab;
            mess.bytes = bytes;
            tempString = System.Text.Encoding.UTF8.GetString(mess.bytes);
            tab = tempString.Split('/');
            mess.sender = tab[0];
            mess.target = tab[1];
            mess.mess = tab[2];
            mess.mess.Trim('\0');
            if (mess.sender == "info")
            {
                updateChatBox("INFO :" + mess.mess);
                return;
            }
            if (mess.target == textPseudo.Text)
            {
                updateChatBox("Private message from " + mess.sender + " : " +mess.mess);
                return;
            }
            if (mess.target == textCanal.Text)
            {
                updateChatBox("message from " + mess.target + " : " + mess.sender + " : " + mess.mess);
                return;
            }
            return;
        }
    }

    public class message
    {
        public String mess;
        public String sender;
        public String target;
        public Byte[] bytes;

        public message(string mess, string sender, string target)
        {
            this.mess = mess;
            this.sender = sender;
            this.target = target;
        }
        public message()
        {

        }

        public void sendMessage(Socket socket)
        {
            String sendObject;
            if (this.mess.Contains('/'))
            {
                return;
            }
            sendObject = sender + "/" + target + "/" + mess;
            bytes = System.Text.Encoding.UTF8.GetBytes(sendObject);
            int send = socket.Send(bytes);
        }


    }

}
