﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace projetCsharpServer
{
    public class Serveur
    {
        static int port = 8080;
        public static List<Client> ListesClient;
        public static Dictionary<String, String> Auth = new Dictionary<string, string>();
        public static string AuthFile = @"/auth";
        static void Main(string[] args)
        {
            try { Auth = (Dictionary<String,String>)serialTool.Deserialize(AuthFile); }
            catch(Exception e) { }
           
            Socket serveurSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint endp = new IPEndPoint(IPAddress.Any, port);

            ListesClient = new List<Client>();
            serveurSocket.Bind(endp);
            serveurSocket.Listen(1);
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(CurrentDomain_ProcessExit);
            Console.WriteLine("socket serveur initialize on port :" + port);

            while (true)
            {
                Console.WriteLine("Wait for client ...");
                Socket sendSocket = serveurSocket.Accept();
                Serveur.connexionTreet(sendSocket);
                
            }
        }

        static void connexionTreet(Socket sendSocket)
        {
            Console.WriteLine("client Socket connected");
            Client nouveauClient = new Client(sendSocket);
            ListesClient.Add(nouveauClient);
            Thread clientThread = new Thread(nouveauClient.clientTreet);
            clientThread.Start();
        }

        public static void Broadcast(String message)
        {
            Console.WriteLine("BROADCAST : " + message);
            foreach(Client client in ListesClient)
            {
                client.sendMessage(message);
            }
        }
        public static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            serialTool.Serialize(AuthFile, Auth);
        }

    }

    public class Client
    {
        private Socket _clientSocket;
        private String _pseudo;
        private String _mdp;
        private String tempString;
        private String[] tab;

        public Client(Socket clientSocket)
        {
            _clientSocket = clientSocket;
        }

        public void clientTreet()
        {
            Console.WriteLine("Thread client lancé.");
            Byte[] buffer = new Byte[255];

            int recu;
            try
            {
                recu = _clientSocket.Receive(buffer);
            }
            catch(Exception ex)
            {
                Console.WriteLine("error during Auth receive");
                return;
            }
            tempString = System.Text.Encoding.UTF8.GetString(buffer);
            tab = tempString.Split('/');
            _pseudo = tab[0];
            _mdp = tab[1];
            _mdp.Trim('\0');
            try
            {
                if(_mdp == Serveur.Auth[_pseudo])
                {
                    sendMessage("info/" + _pseudo + "/authentified");
                }
                else
                {
                    sendMessage("info/" + _pseudo + "/authentification failed");
                    Serveur.ListesClient.Remove(this);
                    _clientSocket.Close();
                }
            }
            catch (Exception e)
            {
                Serveur.Auth.Add(_pseudo, _mdp);
            }


            Serveur.Broadcast(_pseudo + " est entrée dans le chat\0");

            while (_clientSocket.Connected)
            {
                try
                {
                    String message;
                    recu = _clientSocket.Receive(buffer);
                    message = System.Text.Encoding.UTF8.GetString(buffer);
                    message = message.Trim('\0');
                    Serveur.Broadcast(message);
                }
                catch(Exception ex)
                {
                    Serveur.ListesClient.Remove(this);
                    _clientSocket.Close();
                    Serveur.Broadcast(_pseudo + " disconnected");
                    return;
                }
            }
        }
        public void sendMessage(String mess)
        {
            Byte[] message = System.Text.Encoding.UTF8.GetBytes(mess);
            int send = _clientSocket.Send(message);
            Console.WriteLine(send + " bytes envoyés au client " + _pseudo);
        }
    }

    public class serialTool
    {
        public static void Serialize(string filename, object o)
        {
            FileStream fs = new FileStream(filename, FileMode.Create);
            BinaryFormatter binaryf = new BinaryFormatter();
            binaryf.Serialize(fs, o);
            fs.Close();
        }
        public static object Deserialize(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open);
            BinaryFormatter binaryf = new BinaryFormatter();
            object o = binaryf.Deserialize(fs);
            fs.Close();
            return o;
        }
    }
}
